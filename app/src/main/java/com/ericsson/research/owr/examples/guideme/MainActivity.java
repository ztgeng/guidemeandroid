package com.ericsson.research.owr.examples.guideme;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ericsson.research.owr.Owr;
import com.ericsson.research.owr.sdk.InvalidDescriptionException;
import com.ericsson.research.owr.sdk.RtcCandidate;
import com.ericsson.research.owr.sdk.RtcCandidates;
import com.ericsson.research.owr.sdk.RtcConfig;
import com.ericsson.research.owr.sdk.RtcConfigs;
import com.ericsson.research.owr.sdk.RtcSession;
import com.ericsson.research.owr.sdk.RtcSessions;
import com.ericsson.research.owr.sdk.SessionDescription;
import com.ericsson.research.owr.sdk.SessionDescriptions;
import com.ericsson.research.owr.sdk.SimpleStreamSet;
import com.ericsson.research.owr.sdk.VideoView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MainActivity extends FragmentActivity implements
        OnMapReadyCallback,
        SignalingChannel.JoinListener,
        SignalingChannel.DisconnectListener,
        SignalingChannel.SessionFullListener,
        SignalingChannel.MessageListener,
        SignalingChannel.PeerDisconnectListener,
        RtcSession.OnLocalCandidateListener,
        RtcSession.OnLocalDescriptionListener
{

    private static final String TAG = "==GuideMe==";
    private static final boolean wantAudio = false;
    private static final boolean wantVideo = true;

    private Marker curLocationMarker;

    /**
     * Initialize OpenWebRTC at startup
     */
    static {
        Log.d(TAG, "Initializing OpenWebRTC");
        Owr.init();
        Owr.runInBackground();
    }

    private GoogleMap mMap;

    private Button mJoinButton;
    private Button mCallButton;
    private EditText mSessionInput;
    private InputMethodManager mInputMethodManager;
    private WindowManager mWindowManager;
    private RtcConfig mRtcConfig;
    private SignalingChannel mSignalingChannel;
    private SimpleStreamSet mStreamSet;
    private VideoView mRemoteView;
    private SignalingChannel.PeerChannel mPeerChannel;
    private RtcSession mRtcSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initUi();

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mRtcConfig = RtcConfigs.defaultConfig(Config.STUN_SERVER);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sf = new LatLng(35.75, -122.43);
        curLocationMarker = mMap.addMarker(new MarkerOptions().position(sf).title("User Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sf));
    }

    /**
     * OpenWebRTC Starts Here!
     */

    public void initUi() {
        mCallButton = (Button) findViewById(R.id.call);
        mJoinButton = (Button) findViewById(R.id.join);
        mSessionInput = (EditText) findViewById(R.id.session_id);
        mJoinButton.setEnabled(true);
    }

    private void updateVideoView(boolean running) {
        if (mStreamSet != null) {
            TextureView remoteView = (TextureView) findViewById(R.id.remote_view);
            remoteView.setVisibility(running ? View.VISIBLE : View.INVISIBLE);
            if (running) {
                mRemoteView.setView(remoteView);
            } else {
                mRemoteView.stop();
            }
        }
    }

    public void onJoinClicked(final View view) {
        Log.d(TAG, "onJoinClicked");

        String sessionId = mSessionInput.getText().toString();
        if (sessionId.isEmpty()) {
            mSessionInput.requestFocus();
            mInputMethodManager.showSoftInput(mSessionInput, InputMethodManager.SHOW_IMPLICIT);
            return;
        }

        mInputMethodManager.hideSoftInputFromWindow(mSessionInput.getWindowToken(), 0);
        mSessionInput.setEnabled(false);
        mJoinButton.setEnabled(false);

        mSignalingChannel = new SignalingChannel(Config.DEFAULT_SERVER_ADDRESS, sessionId);
        mSignalingChannel.setJoinListener(this);
        mSignalingChannel.setDisconnectListener(this);
        mSignalingChannel.setSessionFullListener(this);

        mStreamSet = SimpleStreamSet.defaultConfig(wantAudio, wantVideo);
        mRemoteView = mStreamSet.createRemoteView();

        mRemoteView.setRotation((mRemoteView.getRotation() + 2) % 4);
        updateVideoView(true);
    }

    public void onCallClicked(final View view) {
        Log.d(TAG, "onCallClicked");

        mRtcSession.start(mStreamSet);
        mCallButton.setEnabled(false);
    }

    @Override
    public void onDisconnect() {
        Toast.makeText(this, "Disconnected from server", Toast.LENGTH_LONG).show();
        updateVideoView(false);
        mStreamSet = null;
        mRtcSession.stop();
        mRtcSession = null;
        mSignalingChannel = null;
    }

    @Override
    public void onPeerJoin(SignalingChannel.PeerChannel peerChannel) {
        Log.v(TAG, "onPeerJoin => " + peerChannel.getPeerId());
        mCallButton.setEnabled(true);
        mPeerChannel = peerChannel;
        mPeerChannel.setDisconnectListener(this);
        mPeerChannel.setMessageListener(this);

        mRtcSession = RtcSessions.create(mRtcConfig);
        mRtcSession.setOnLocalCandidateListener(this);
        mRtcSession.setOnLocalDescriptionListener(this);
    }

    @Override
    public void onMessage(final JSONObject json) {
        if (json.has("candidate")) {
            JSONObject candidate = json.optJSONObject("candidate");
            Log.v(TAG, "candidate: " + candidate);
            RtcCandidate rtcCandidate = RtcCandidates.fromJsep(candidate);
            if (rtcCandidate != null) {
                mRtcSession.addRemoteCandidate(rtcCandidate);
            } else {
                Log.w(TAG, "invalid candidate: " + candidate);
            }
        }
        if (json.has("sdp")) {
            JSONObject sdp = json.optJSONObject("sdp");
            Log.v(TAG, "sdp: " + sdp);
            try {
                SessionDescription sessionDescription = SessionDescriptions.fromJsep(sdp);
                if (sessionDescription.getType() == SessionDescription.Type.OFFER) {
                    onInboundCall(sessionDescription);
                } else {
                    onAnswer(sessionDescription);
                }
            } catch (InvalidDescriptionException e) {
                e.printStackTrace();
            }
        }
//        if (json.has("orientation")) {
//                handleOrientation(json.getInt("orientation"));
//        }
        // For testing only
        if (json.has("num")) {
            try {
                String numStr = json.getString("num");
                System.out.println("=================== receive number: " + numStr);
                Toast.makeText(this, numStr, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        // Move the marker
        if (json.has("lat")) {
            try {
                double latitude = Double.parseDouble(json.getString("lat"));
                double longitude = Double.parseDouble(json.getString("lng"));
                LatLng newLatLng = new LatLng(latitude, longitude);
                curLocationMarker.setPosition(newLatLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Called when a local candidate is generated.
     *
     * @param candidate a local RtcCandidate that should be sent to the other peer.
     */
    @Override
    public void onLocalCandidate(RtcCandidate candidate) {
        if (mPeerChannel != null) {
            try {
                JSONObject json = new JSONObject();
                json.putOpt("candidate", RtcCandidates.toJsep(candidate));
                json.getJSONObject("candidate").put("sdpMid", "video");
                Log.d(TAG, "sending candidate: " + json);
                mPeerChannel.send(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Called once the local description is ready to be sent to the peer.
     *
     * @param localDescription a SessionDescription that should be sent to the other peer.
     */
    @Override
    public void onLocalDescription(SessionDescription localDescription) {
        if (mPeerChannel != null) {
            try {
                JSONObject json = new JSONObject();
                json.putOpt("sdp", SessionDescriptions.toJsep(localDescription));
                Log.d(TAG, "sending sdp: " + json);
                mPeerChannel.send(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPeerDisconnect(SignalingChannel.PeerChannel peerChannel) {
        Log.d(TAG, "onPeerDisconnect => " + peerChannel.getPeerId());
        mRtcSession.stop();
        mPeerChannel = null;
        updateVideoView(false);
        mSessionInput.setEnabled(true);
        mJoinButton.setEnabled(true);
        mCallButton.setEnabled(false);
    }

    @Override
    public void onSessionFull() {
        Toast.makeText(this, "Session is full", Toast.LENGTH_LONG).show();
        mJoinButton.setEnabled(true);
    }

    private void onInboundCall(final SessionDescription sessionDescription) {
        Log.d(TAG, "---------------InboundCall--------------");
        try {
            mRtcSession.setRemoteDescription(sessionDescription);
            mRtcSession.start(mStreamSet);
        } catch (InvalidDescriptionException e) {
            e.printStackTrace();
        }
    }

    private void onAnswer(final SessionDescription sessionDescription) {
        Log.d(TAG, "------------------Answer----------------");
        if (mRtcSession != null) {
            try {
                mRtcSession.setRemoteDescription(sessionDescription);
            } catch (InvalidDescriptionException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Shutdown the process as a workaround until cleanup has been fully implemented.
     */
    @Override
    protected void onStop() {
        super.onStop();
        finish();
        System.exit(0);
    }


    public void onSendTest(final View view) {
        if (mPeerChannel != null) {
            try {
                JSONObject json = new JSONObject();
//                json.putOpt("num","1234567");
//                Log.d(TAG, "sending num" + json);
                Random random = new Random();
                double latitude  = random.nextDouble() * 0.1 + 37.75;
                double longitude = random.nextDouble() * 0.1 - 122.44;
                json.putOpt("lat", String.valueOf(latitude));
                json.putOpt("lng", String.valueOf(longitude));
                mPeerChannel.send(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
